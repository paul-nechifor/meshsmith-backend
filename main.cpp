#include <iostream>
#include <string>
#include <boost/json/src.hpp>
#include "src/json.h"
#include "src/transform_mesh.h"
#include "src/json_output.h"

int default_n_faces = 12;
bool debug = false;
std::string output_format = "off";
std::string version = "0.0.1";

void process_env_vars() {
    auto str_n_faces = std::getenv("MESHSMITH_N_FACES");
    if (str_n_faces != nullptr) {
        default_n_faces = std::stoi(str_n_faces);
    }

    auto str_debug = std::getenv("MESHSMITH_DEBUG");
    if (str_debug != nullptr) {
        debug = strlen(str_debug) > 0;
    }

    auto str_format = std::getenv("MESHSMITH_FORMAT");
    if (str_format != nullptr) {
        output_format = str_format;
        std::transform(output_format.begin(), output_format.end(), output_format.begin(),
                       [](unsigned char c) { return std::tolower(c); });
    }
}

void process_args(int argc, char** argv) {
    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];

        if (arg == "-h" || arg == "--help") {
            std::cout << "meshsmith-backend (version " << version << ")\n\n"
                      << "Usage options:\n"
                      << " -h, --help     Show this help message.\n"
                      << " -v, --version  Show the version.\n"
                      << " -f, --format   Format option (json, off, obj).\n"
                      << " -d, --debug    Enable debug mode.\n";
            exit(EXIT_SUCCESS);
        } else if (arg == "-v" || arg == "--version") {
            std::cout << version << std::endl;
            exit(EXIT_SUCCESS);
        } else if (arg == "-f" || arg == "--format") {
            if (i + 1 < argc) {
                i++;
                output_format = argv[i];
            } else {
                std::cerr << "--format option requires one argument." << std::endl;
                exit(EXIT_FAILURE);
            }
        } else if (arg == "-d" || arg == "--debug") {
            debug = true;
        } else {
            std::cerr << "Unknown option: " << arg << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void validate_args() {
    if (output_format != "off" && output_format != "json" && output_format != "obj") {
        std::cerr << "Invalid format: " << output_format << ". Use 'off', 'json', or 'obj'." << std::endl;
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char** argv) {
    std::ios_base::sync_with_stdio(false);

    process_env_vars();

    process_args(argc, argv);

    validate_args();

    if (debug) {
        std::cerr << "Running in debug mode.\n" << std::endl;
        std::cerr << "Options:" << std::endl;
        std::cerr << "  - default_n_faces: " << default_n_faces << std::endl;
        std::cerr << "  - output_format: " << output_format << std::endl;
    }

    json::value input;

    try {
        input = parse_file();
    } catch(std::exception const& e) {
        std::cerr << "Caught exception: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    Mesh mesh;

    transform_mesh(mesh, input);

    if (output_format == "off") {
        std::cout << mesh;
    } else if (output_format == "json") {
        print_as_json(mesh);
    } else {
        std::cerr << "TODO: implement this format" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
