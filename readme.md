# Meshsmith Backend

This is the C++ backend for Meshsmith. It uses CGAL.

## Usage

Install dependencies:

    sudo apt install libmpfr-dev

Compile:

    ./compile

The binary will be in `build/meshsmith-backend`.

Run the tests:

    ./test

Or just the tests which start with a string:

    ./test diff

## Viewer

You can use f3d:

    wget https://github.com/f3d-app/f3d/releases/download/v1.3.1/f3d-1.3.1-Linux.deb
    sudo dpkg -i f3d-1.3.1-Linux.deb
    rm f3d-1.3.1-Linux.deb

    f3d tests/difference-01.off

## Notes

- Paths must always be anticlockwise.

- Z is the up axis.
