#include "transform_extrude_path.h"

#include "globals.h"
#include "utils_extrusion.h"

void transform_extrude_path(Mesh &m, json::value &input) {
    // Careful, the order of the points matters and the shape must include 0,0
    // inside it.
    auto obj = input.get_object();

    if (obj["path"] == nullptr) {
        std::cerr << "This ExtrudePath should have 'path': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    int n_faces = get_as_int_or(obj["n_faces"], default_n_faces);

    if (obj["height"] == nullptr) {
        std::cerr << "This ExtrudePath should have 'height': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto path = obj["path"].get_array();

    auto height = get_as_double(obj["height"]);

    std::vector<std::vector<double>> points_vec;
    auto it = path.begin();
    std::vector<double> last_point = {0, 0};
    for (int i = 0; ; i++) {
        auto pair = (*it).as_array();
        std::string command = pair[0].get_string().c_str();
        if (i == 0 && command != "M") {
            std::cerr << "The first command in the ExtrudePath must be an M: ";
            pretty_print(std::cerr, input);
            exit(EXIT_FAILURE);
        }
        if (i > 0 && command == "M") {
            std::cerr << "Using M past the first commmand in ExtrudePath is not supported: ";
            pretty_print(std::cerr, input);
            exit(EXIT_FAILURE);
        }

        if (command == "M" || command == "L") {
            last_point = { get_as_double(pair[1]), get_as_double(pair[2]) };
            points_vec.push_back({get_as_double(pair[1]), get_as_double(pair[2])});
        } else if (command == "C") {
            add_bezier_points(
                points_vec,
                n_faces,
                last_point[0],
                last_point[1],
                get_as_double(pair[1]),
                get_as_double(pair[2]),
                get_as_double(pair[3]),
                get_as_double(pair[4]),
                get_as_double(pair[5]),
                get_as_double(pair[6])
            );
            last_point = { get_as_double(pair[5]), get_as_double(pair[6]) };
        } else if (command == "Q") {
            add_quadratic_bezier_points(
                points_vec,
                n_faces,
                last_point[0],
                last_point[1],
                get_as_double(pair[1]),
                get_as_double(pair[2]),
                get_as_double(pair[3]),
                get_as_double(pair[4])
            );
            last_point = { get_as_double(pair[3]), get_as_double(pair[4]) };
        } else {
            std::cerr << "Don't know how to handle command " << command << " in this ExtrudePath: ";
            pretty_print(std::cerr, input);
            exit(EXIT_FAILURE);
        }

        if (++it == path.end()) {
            break;
        }
    }

    extrude_from_points(m, points_vec, height);
}
