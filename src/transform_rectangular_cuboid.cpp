#include "transform_rectangular_cuboid.h"

void transform_rectangular_cuboid(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["p1"] == nullptr) {
        std::cerr << "This RectangularCuboid should have 'p1': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["p2"] == nullptr) {
        std::cerr << "This RectangularCuboid should have 'p2': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto p1 = obj["p1"].get_array();
    auto p2 = obj["p2"].get_array();

    double p1x = get_as_double(p1[0]);
    double p1y = get_as_double(p1[1]);
    double p1z = get_as_double(p1[2]);
    double p2x = get_as_double(p2[0]);
    double p2y = get_as_double(p2[1]);
    double p2z = get_as_double(p2[2]);

    auto a = m.add_vertex(K::Point_3(p1x, p1y, p1z));
    auto b = m.add_vertex(K::Point_3(p2x, p1y, p1z));
    auto c = m.add_vertex(K::Point_3(p2x, p2y, p1z));
    auto d = m.add_vertex(K::Point_3(p1x, p2y, p1z));

    auto e = m.add_vertex(K::Point_3(p1x, p1y, p2z));
    auto f = m.add_vertex(K::Point_3(p2x, p1y, p2z));
    auto g = m.add_vertex(K::Point_3(p2x, p2y, p2z));
    auto h = m.add_vertex(K::Point_3(p1x, p2y, p2z));

    m.add_face(a, c, b);
    m.add_face(a, d, c);
    m.add_face(a, b, f);

    m.add_face(a, f, e);
    m.add_face(b, c, g);
    m.add_face(b, g, f);

    m.add_face(c, d, h);
    m.add_face(c, h, g);
    m.add_face(d, a, e);

    m.add_face(d, e, h);
    m.add_face(e, f, g);
    m.add_face(e, g, h);
}
