#include "transform_difference.h"

#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include "transform_mesh.h"
#include "utils_validation.h"
#include "globals.h"

void transform_difference(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh1"] == nullptr) {
        std::cerr << "This Difference should have 'mesh1': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["mesh2"] == nullptr) {
        std::cerr << "This Difference should have 'mesh2': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    Mesh mesh1;
    transform_mesh(mesh1, obj["mesh1"]);

    Mesh mesh2;
    transform_mesh(mesh2, obj["mesh2"]);

    if (obj["join"] != nullptr) {
        std::string join = obj["join"].get_string().c_str();
        if (!is_valid_join(join)) {
            std::cerr << "This join is invalid: " << join;
            exit(EXIT_FAILURE);
        }

        std::string sub = join.substr(1);
        make_nudge(join[0] == '1' ? mesh1 : mesh2, sub);
    }

    bool valid = CGAL::Polygon_mesh_processing::corefine_and_compute_difference(mesh1, mesh2, m);

    if (!valid) {
        std::cerr << "Failed to compute this Difference: ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }
}
