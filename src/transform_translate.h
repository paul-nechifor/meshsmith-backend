#ifndef MESHSMITH_BACKEND_TRANSFORM_TRANSLATE_H
#define MESHSMITH_BACKEND_TRANSFORM_TRANSLATE_H

#include "json.h"
#include "typedefs.h"

void transform_translate(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_TRANSLATE_H
