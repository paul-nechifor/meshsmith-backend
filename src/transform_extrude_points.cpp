#include "transform_extrude_points.h"

#include "utils_extrusion.h"

void transform_extrude_points(Mesh &m, json::value &input) {
    // Careful, the order of the points matters and the shape must include 0,0
    // inside it.
    auto obj = input.get_object();

    if (obj["points"] == nullptr) {
        std::cerr << "This ExtrudePoints should have 'points': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["height"] == nullptr) {
        std::cerr << "This ExtrudePoints should have 'height': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto points = obj["points"].get_array();

    auto height = get_as_double(obj["height"]);

    std::vector<std::vector<double>> points_vec;
    auto it = points.begin();
    for (;;) {
        auto pair = (*it).as_array();
        points_vec.push_back({get_as_double(pair[0]), get_as_double(pair[1])});
        if (++it == points.end()) {
            break;
        }
    }

    extrude_from_points(m, points_vec, height);
}
