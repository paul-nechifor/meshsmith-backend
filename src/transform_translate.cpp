#include "transform_translate.h"

#include "transform_mesh.h"

void transform_translate(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh"] == nullptr) {
        std::cerr << "This Translate should have 'mesh': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["p"] == nullptr) {
        std::cerr << "This Translate should have 'p': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    transform_mesh(m, obj["mesh"]);

    auto p = obj["p"].get_array();
    double px = get_as_double(p[0]);
    double py = get_as_double(p[1]);
    double pz = get_as_double(p[2]);

    for (auto vi : m.vertices()) {
        auto pi = m.point(vi);
        m.point(vi) = K::Point_3(
                pi.x() + px,
                pi.y() + py,
                pi.z() + pz
        );
    }
}
