#ifndef MESHSMITH_BACKEND_UTILS_VALIDATION_H
#define MESHSMITH_BACKEND_UTILS_VALIDATION_H

#include "typedefs.h"

bool is_valid_join(std::string &join);
bool is_valid_move(std::string &move);
void make_nudge(Mesh &m, std::string &move);

#endif //MESHSMITH_BACKEND_UTILS_VALIDATION_H
