#ifndef MESHSMITH_BACKEND_TRANSFORM_SIMPLIFY_H
#define MESHSMITH_BACKEND_TRANSFORM_SIMPLIFY_H

#include "json.h"
#include "typedefs.h"

void transform_simplify(Mesh &mesh, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_SIMPLIFY_H
