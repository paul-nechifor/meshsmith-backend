#include "transform_torus.h"

#include "globals.h"

void transform_torus(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["width"] == nullptr) {
        std::cerr << "This Torus should have 'width': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["thickness"] == nullptr) {
        std::cerr << "This Torus should have 'thickness': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto width = get_as_double(obj["width"]) / 2;
    auto thickness = get_as_double(obj["thickness"]) / 2;
    int n_faces = get_as_int_or(obj["n_faces"], default_n_faces);

    auto theta = (M_PI * 2) / n_faces;

    for (int i = 0; i < n_faces; i++) {
        for (int j = 0; j < n_faces; j++) {
            double tmp = (width + thickness * std::cos(j * theta));
            m.add_vertex(K::Point_3(
                    tmp * std::cos(i * theta),
                    tmp * std::sin(i * theta),
                    thickness * std::sin(j * theta)
            ));
        }
    }

    for (int i = 0; i < n_faces; i++) {
        for (int j = 0; j < n_faces; j++) {
            m.add_face(
                    VI(i * n_faces + j),
                    VI((i * n_faces + n_faces + j) % (n_faces * n_faces)),
                    VI(i * n_faces + (j + 1) % n_faces)
            );
            m.add_face(
                    VI(i * n_faces + (j + 1) % n_faces),
                    VI((i * n_faces + n_faces + j) % (n_faces * n_faces)),
                    VI((i * n_faces + ((j + 1) % n_faces) + n_faces) % (n_faces * n_faces))
            );
        }
    }
}
