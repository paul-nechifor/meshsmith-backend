#ifndef MESHSMITH_BACKEND_TRANSFORM_VERTICES_AND_FACES_H
#define MESHSMITH_BACKEND_TRANSFORM_VERTICES_AND_FACES_H

#include "json.h"
#include "typedefs.h"

void transform_vertices_and_faces(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_VERTICES_AND_FACES_H
