#include "transform_intersection.h"

#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include "transform_mesh.h"

void transform_intersection(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh1"] == nullptr) {
        std::cerr << "This Intersection should have 'mesh1': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["mesh2"] == nullptr) {
        std::cerr << "This Intersection should have 'mesh2': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    Mesh mesh1;
    transform_mesh(mesh1, obj["mesh1"]);

    Mesh mesh2;
    transform_mesh(mesh2, obj["mesh2"]);

    bool valid = CGAL::Polygon_mesh_processing::corefine_and_compute_intersection(mesh1, mesh2, m);

    if (!valid) {
        std::cerr << "Failed to compute this Intersection: ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }
}
