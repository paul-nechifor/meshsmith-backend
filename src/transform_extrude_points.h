#ifndef MESHSMITH_BACKEND_TRANSFORM_EXTRUDE_POINTS_H
#define MESHSMITH_BACKEND_TRANSFORM_EXTRUDE_POINTS_H

#include "json.h"
#include "typedefs.h"

void transform_extrude_points(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_EXTRUDE_POINTS_H
