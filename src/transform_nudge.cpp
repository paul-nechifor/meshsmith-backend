#include "transform_nudge.h"

#include "utils_validation.h"
#include "transform_mesh.h"

void transform_nudge(Mesh &mesh, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh"] == nullptr) {
        std::cerr << "This Nudge should have 'mesh': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["move"] == nullptr) {
        std::cerr << "This Nudge should have 'move': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    std::string move = obj["move"].get_string().c_str();
    if (!is_valid_move(move)) {
        std::cerr << "This move is invalid: " << move;
        exit(EXIT_FAILURE);
    }

    transform_mesh(mesh, obj["mesh"]);

    make_nudge(mesh, move);
}
