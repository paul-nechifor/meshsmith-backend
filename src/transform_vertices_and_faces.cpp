#include "transform_vertices_and_faces.h"

void transform_vertices_and_faces(Mesh &m, json::value &input) {
    auto obj = input.get_object();
    auto vertices = obj["vertices"].get_array();
    auto faces = obj["faces"].get_array();


    auto it = vertices.begin();
    for (;;) {
        auto vertex = (*it).as_array();
        m.add_vertex(K::Point_3(
                get_as_double(vertex[0]),
                get_as_double(vertex[1]),
                get_as_double(vertex[2])
        ));
        if (++it == vertices.end()) {
            break;
        }
    }

    it = faces.begin();
    for (;;) {
        auto face = (*it).as_array();
        m.add_face(
                VI(get_as_int(face[0])),
                VI(get_as_int(face[1])),
                VI(get_as_int(face[2]))
        );
        if (++it == faces.end()) {
            break;
        }
    }
}