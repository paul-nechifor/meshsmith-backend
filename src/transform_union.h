#ifndef MESHSMITH_BACKEND_TRANSFORM_UNION_H
#define MESHSMITH_BACKEND_TRANSFORM_UNION_H

#include "json.h"
#include "typedefs.h"

void transform_union(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_UNION_H
