#ifndef MESHSMITH_BACKEND_JSON_H
#define MESHSMITH_BACKEND_JSON_H

#include <iostream>
#include <boost/json.hpp>

namespace json = boost::json;

json::value parse_file();
void pretty_print(std::ostream& os, json::value const& jv, std::string* indent = nullptr);
double get_as_double(json::value &v);
double get_as_double_or(json::value &v, double default_value);
double get_as_int(json::value &v);
double get_as_int_or(json::value &v, int default_value);

#endif //MESHSMITH_BACKEND_JSON_H
