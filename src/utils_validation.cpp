#include "utils_validation.h"

bool is_valid_join(std::string &join) {
    if (join.length() != 3) {
        return false;
    }
    if (join[0] != '1' && join[0] != '2') {
        return false;
    }
    auto sub = join.substr(1);
    return is_valid_move(sub);
}

bool is_valid_move(std::string &move) {
    if (move.length() != 2) {
        return false;
    }
    if (move[0] != 'x' && move[0] != 'y' && move[0] != 'z') {
        return false;
    }
    if (move[1] != '+' && move[1] != '-') {
        return false;
    }
    return true;
}

void make_nudge(Mesh &m, std::string &move) {
    auto axis = move[0];
    auto operation = move[1];

    // It cannot be smaller or it gets rounded off. I don't know why.
    double epsilon = 0.00001;

    double result = (operation == '+') ? std::numeric_limits<double>::lowest() : std::numeric_limits<double>::max();

    for (auto vi : m.vertices()) {
        auto pi = m.point(vi);
        auto value = axis == 'x' ? pi.x() : (axis == 'y' ? pi.y() : pi.z());
        if ((operation == '+' && value > result) || (operation == '-' && value < result)) {
            result = value;
        }
    }

    for (auto vi : m.vertices()) {
        auto pi = m.point(vi);
        m.point(vi) = K::Point_3(
                (axis == 'x' && pi.x() == result) ? pi.x() + epsilon : pi.x(),
                (axis == 'y' && pi.y() == result) ? pi.y() + epsilon : pi.y(),
                (axis == 'z' && pi.z() == result) ? pi.z() + epsilon : pi.z()
        );
    }
}
