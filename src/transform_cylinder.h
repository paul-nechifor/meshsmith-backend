#ifndef MESHSMITH_BACKEND_TRANSFORM_CYLINDER_H
#define MESHSMITH_BACKEND_TRANSFORM_CYLINDER_H

#include "typedefs.h"
#include "json.h"

void transform_cylinder(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_CYLINDER_H
