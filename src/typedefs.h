#ifndef MESHSMITH_BACKEND_TYPEDEFS_H
#define MESHSMITH_BACKEND_TYPEDEFS_H

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Surface_mesh<K::Point_3> Mesh;
typedef Mesh::Vertex_index VI;

#endif //MESHSMITH_BACKEND_TYPEDEFS_H
