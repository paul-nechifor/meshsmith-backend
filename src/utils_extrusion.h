#ifndef MESHSMITH_BACKEND_UTILS_EXTRUSION_H
#define MESHSMITH_BACKEND_UTILS_EXTRUSION_H

#include "typedefs.h"

void extrude_from_points(Mesh &m, std::vector<std::vector<double>> &points, double height);
void add_bezier_points(
    std::vector<std::vector<double>> &points,
    int n,
    double p0x,
    double p0y,
    double p1x,
    double p1y,
    double p2x,
    double p2y,
    double p3x,
    double p3y
);
void add_quadratic_bezier_points(
    std::vector<std::vector<double>> &points,
    int n,
    double p0x,
    double p0y,
    double p1x,
    double p1y,
    double p2x,
    double p2y
);

#endif //MESHSMITH_BACKEND_UTILS_EXTRUSION_H
