#include "transform_cone.h"

#include "globals.h"

void transform_cone(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["radius"] == nullptr) {
        std::cerr << "This Cone should have 'radius': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["height"] == nullptr) {
        std::cerr << "This Cone should have 'height': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto radius = get_as_double(obj["radius"]);
    auto height = get_as_double(obj["height"]);
    int n_faces = get_as_int_or(obj["n_faces"], default_n_faces);

    auto theta = (M_PI * 2) / n_faces;

    for (int i = 0; i < n_faces; i++) {
        m.add_vertex(K::Point_3(radius * std::cos(theta * i), radius * std::sin(theta * i), 0));
    }

    m.add_vertex(K::Point_3(0, 0, 0));
    m.add_vertex(K::Point_3(0, 0, height));

    for (int i = 0; i < n_faces; i++) {
        m.add_face(VI(i), VI(n_faces), VI((i + 1) % n_faces));
        m.add_face(VI(i), VI((i + 1) % n_faces), VI(n_faces + 1));
    }
}
