#include "json_output.h"

#include "json.h"

void print_as_json(Mesh &mesh) {
    std::vector<std::vector<double>> vertices;
    std::vector<std::vector<uint32_t>> faces;

    for (Mesh::Vertex_index vi : mesh.vertices()) {
        K::Point_3 pt = mesh.point(vi);
        vertices.push_back({pt.x(), pt.y(), pt.z()});
    }

    for (Mesh::Face_index face_index : mesh.faces()) {
        CGAL::Vertex_around_face_circulator<Mesh> vcirc(mesh.halfedge(face_index), mesh), done(vcirc);
        std::vector<uint32_t> face;
        do {
            face.push_back(*vcirc++);
        } while (vcirc != done);
        faces.push_back(face);
    }

    json::object obj;
    obj["vertices"] = json::value_from(vertices);
    obj["faces"] = json::value_from(faces);
    std::cout << json::serialize(obj) << std::endl;
}
