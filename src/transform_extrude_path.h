#ifndef MESHSMITH_BACKEND_TRANSFORM_EXTRUDE_PATH_H
#define MESHSMITH_BACKEND_TRANSFORM_EXTRUDE_PATH_H

#include "json.h"
#include "typedefs.h"

void transform_extrude_path(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_EXTRUDE_PATH_H
