#ifndef MESHSMITH_BACKEND_JSON_OUTPUT_H
#define MESHSMITH_BACKEND_JSON_OUTPUT_H

#include "typedefs.h"

void print_as_json(Mesh &mesh);

#endif //MESHSMITH_BACKEND_JSON_OUTPUT_H
