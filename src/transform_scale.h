#ifndef MESHSMITH_BACKEND_TRANSFORM_SCALE_H
#define MESHSMITH_BACKEND_TRANSFORM_SCALE_H

#include "json.h"
#include "typedefs.h"

void transform_scale(Mesh &mesh, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_SCALE_H
