#ifndef MESHSMITH_BACKEND_TRANSFORM_CONE_H
#define MESHSMITH_BACKEND_TRANSFORM_CONE_H

#include "typedefs.h"
#include "json.h"

void transform_cone(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_CONE_H
