#include "transform_cylinder.h"

#include "globals.h"

void transform_cylinder(Mesh &m, json::value &input) {
    auto obj = input.get_object();

    if (obj["radius"] == nullptr) {
        std::cerr << "This Cylinder should have 'radius': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["height"] == nullptr) {
        std::cerr << "This Cylinder should have 'height': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    int n_faces = get_as_int_or(obj["n_faces"], default_n_faces);

    auto radius = get_as_double(obj["radius"]);
    auto height = get_as_double(obj["height"]);

    auto theta = (M_PI * 2) / n_faces;

    for (int i = 0; i < n_faces; i++) {
        m.add_vertex(K::Point_3(radius * std::cos(theta * i), radius * std::sin(theta * i), 0));
    }

    for (int i = 0; i < n_faces; i++) {
        m.add_vertex(K::Point_3(radius * std::cos(theta * i), radius * std::sin(theta * i), height));
    }

    m.add_vertex(K::Point_3(0, 0, 0));
    m.add_vertex(K::Point_3(0, 0, height));

    for (int i = 0; i < n_faces; i++) {
        // Face
        m.add_face(VI(i), VI((i + 1) % n_faces), VI(i + n_faces));
        m.add_face(VI((i + 1) % n_faces), VI(((i + 1) % n_faces) + n_faces), VI(i + n_faces));
        // Bottom
        m.add_face(VI(i), VI(2 * n_faces), VI((i + 1) % n_faces));
        // Top
        m.add_face(VI(i + n_faces), VI(((i + 1) % n_faces) + n_faces), VI(2 * n_faces + 1));
    }
}
