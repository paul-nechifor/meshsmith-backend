#ifndef MESHSMITH_BACKEND_TRANSFORM_INTERSECTION_H
#define MESHSMITH_BACKEND_TRANSFORM_INTERSECTION_H

#include "json.h"
#include "typedefs.h"

void transform_intersection(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_INTERSECTION_H
