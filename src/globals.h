#ifndef MESHSMITH_BACKEND_GLOBALS_H
#define MESHSMITH_BACKEND_GLOBALS_H

extern int default_n_faces;
extern bool debug;
extern bool print_json;

#endif //MESHSMITH_BACKEND_GLOBALS_H
