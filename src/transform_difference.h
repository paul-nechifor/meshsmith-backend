#ifndef MESHSMITH_BACKEND_TRANSFORM_DIFFERENCE_H
#define MESHSMITH_BACKEND_TRANSFORM_DIFFERENCE_H

#include "json.h"
#include "typedefs.h"

void transform_difference(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_DIFFERENCE_H
