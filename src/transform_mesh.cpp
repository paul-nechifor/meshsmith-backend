#include "transform_mesh.h"

#include <iostream>
#include <cmath>

#include "transform_rectangular_cuboid.h"
#include "transform_vertices_and_faces.h"
#include "transform_rotate.h"
#include "transform_cylinder.h"
#include "transform_cone.h"
#include "transform_torus.h"
#include "transform_scale.h"
#include "transform_translate.h"
#include "transform_simplify.h"
#include "transform_extrude_path.h"
#include "transform_extrude_points.h"
#include "transform_intersection.h"
#include "transform_union.h"
#include "transform_difference.h"
#include "transform_nudge.h"

void transform_mesh(Mesh &m, json::value &input) {
    if (input.kind() != json::kind::object) {
        std::cerr << "This should have been a JSON object: ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto object = input.get_object();

    if (object["vertices"] != nullptr && object["faces"] != nullptr) {
        transform_vertices_and_faces(m, input);
        return;
    }

    auto type = object["type"];

    if (type == "RectangularCuboid") {
        transform_rectangular_cuboid(m, input);
        return;
    }

    if (type == "Cylinder") {
        transform_cylinder(m, input);
        return;
    }

    if (type == "Cone") {
        transform_cone(m, input);
        return;
    }

    if (type == "Torus") {
        transform_torus(m, input);
        return;
    }

    if (type == "Union") {
        transform_union(m, input);
        return;
    }

    if (type == "Intersection") {
        transform_intersection(m, input);
        return;
    }

    if (type == "Difference") {
        transform_difference(m, input);
        return;
    }

    if (type == "Translate") {
        transform_translate(m, input);
        return;
    }

    if (type == "Rotate") {
        transform_rotate(m, input);
        return;
    }

    if (type == "Scale") {
        transform_scale(m, input);
        return;
    }

    if (type == "Simplify") {
        transform_simplify(m, input);
        return;
    }

    if (type == "Nudge") {
        transform_nudge(m, input);
        return;
    }

    if (type == "ExtrudePoints") {
        transform_extrude_points(m, input);
        return;
    }

    if (type == "ExtrudePath") {
        transform_extrude_path(m, input);
        return;
    }

    std::cerr << "This has an unknown type: ";
    pretty_print(std::cerr, input);
    exit(EXIT_FAILURE);
}
