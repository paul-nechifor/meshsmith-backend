#ifndef MESHSMITH_BACKEND_TRANSFORM_ROTATE_H
#define MESHSMITH_BACKEND_TRANSFORM_ROTATE_H

#include "typedefs.h"
#include "json.h"

void transform_rotate(Mesh &mesh, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_ROTATE_H
