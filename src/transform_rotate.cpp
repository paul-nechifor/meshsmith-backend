#include "transform_rotate.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include "transform_mesh.h"

namespace ublas = boost::numeric::ublas;

void transform_rotate(Mesh &mesh, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh"] == nullptr) {
        std::cerr << "This Rotate should have 'mesh': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["theta"] == nullptr) {
        std::cerr << "This Rotate should have 'theta': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["axis"] == nullptr) {
        std::cerr << "This Rotate should have 'axis': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    transform_mesh(mesh, obj["mesh"]);

    double theta = get_as_double(obj["theta"]);
    auto axis = obj["axis"].get_string();

    ublas::matrix<double> m(mesh.number_of_vertices(), 3);
    ublas::matrix<double> r(3, 3);

    int i = 0;
    for (auto vi : mesh.vertices()) {
        auto pi = mesh.point(vi);
        m(i, 0) = pi.x();
        m(i, 1) = pi.y();
        m(i, 2) = pi.z();
        i++;
    }

    if (axis == "x") {
        r(0, 0) = 1;
        r(0, 1) = 0;
        r(0, 2) = 0;
        r(1, 0) = 0;
        r(1, 1) = std::cos(theta);
        r(1, 2) = -std::sin(theta);
        r(2, 0) = 0;
        r(2, 1) = std::sin(theta);
        r(2, 2) = std::cos(theta);
    } else if (axis == "y") {
        r(0, 0) = std::cos(theta);
        r(0, 1) = 0;
        r(0, 2) = std::sin(theta);
        r(1, 0) = 0;
        r(1, 1) = 1;
        r(1, 2) = 0;
        r(2, 0) = -std::sin(theta);
        r(2, 1) = 0;
        r(2, 2) = std::cos(theta);
    } else if (axis == "z") {
        r(0, 0) = std::cos(theta);
        r(0, 1) = -std::sin(theta);
        r(0, 2) = 0;
        r(1, 0) = std::sin(theta);
        r(1, 1) = std::cos(theta);
        r(1, 2) = 0;
        r(2, 0) = 0;
        r(2, 1) = 0;
        r(2, 2) = 1;
    } else {
        std::cerr << "This Rotate has an invalid axis: ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    auto mr = ublas::prod(m, r);

    i = 0;
    for (auto vi : mesh.vertices()) {
        mesh.point(vi) = K::Point_3(
                mr(i, 0),
                mr(i, 1),
                mr(i, 2)
        );
        i++;
    }
}
