#ifndef MESHSMITH_BACKEND_TRANSFORM_NUDGE_H
#define MESHSMITH_BACKEND_TRANSFORM_NUDGE_H

#include "json.h"
#include "typedefs.h"

void transform_nudge(Mesh &mesh, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_NUDGE_H
