#include "transform_scale.h"

#include "transform_mesh.h"

void transform_scale(Mesh &mesh, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh"] == nullptr) {
        std::cerr << "This Scale should have 'mesh': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    double x = get_as_double_or(obj["x"], 1.0);
    double y = get_as_double_or(obj["y"], 1.0);
    double z = get_as_double_or(obj["z"], 1.0);

    // If you mirror (that is, scale negatively), the triangles are oriented
    // the wrong way. So if you mirror 0 times or 2 times, we can keep the
    // triangle order, but for 1, or 3 we have to flip the triangles.
    bool keep_face_order = ((x < 0) + (y < 0) + (z < 0)) % 2 == 0;

    if (keep_face_order) {
        transform_mesh(mesh, obj["mesh"]);

        for (auto vi : mesh.vertices()) {
            auto pi = mesh.point(vi);
            mesh.point(vi) = K::Point_3(
                pi.x() * x,
                pi.y() * y,
                pi.z() * z
            );
        }

        return;
    }

    Mesh tmp;

    transform_mesh(tmp, obj["mesh"]);

    for (auto vi : tmp.vertices()) {
        auto pi = tmp.point(vi);
        mesh.add_vertex(K::Point_3(
            pi.x() * x,
            pi.y() * y,
            pi.z() * z
        ));
    }

    for (Mesh::Face_index face_index : tmp.faces()) {
        CGAL::Vertex_around_face_circulator<Mesh> vcirc(tmp.halfedge(face_index), tmp), done(vcirc);
        std::vector<uint32_t> face;
        do {
            face.push_back(*vcirc++);
        } while (vcirc != done);

        // Flip the triangle order.
        mesh.add_face(VI(face[0]), VI(face[2]), VI(face[1]));
    }
}
