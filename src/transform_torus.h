#ifndef MESHSMITH_BACKEND_TRANSFORM_TORUS_H
#define MESHSMITH_BACKEND_TRANSFORM_TORUS_H

#include "json.h"
#include "typedefs.h"

void transform_torus(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_TORUS_H
