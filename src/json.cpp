#include "json.h"

json::value parse_file() {
    json::stream_parser p;
    json::error_code ec;

    do {
        char buf[4096];
        std::cin.read(buf, sizeof(buf));
        p.write(buf, std::cin.gcount(), ec);
    } while (std::cin);

    if (ec) {
        return nullptr;
    }

    p.finish(ec);

    if (ec) {
        return nullptr;
    }

    return p.release();
}

void
pretty_print(std::ostream& os, json::value const& jv, std::string* indent) {
    std::string indent_;

    if (!indent) {
        indent = &indent_;
    }

    switch (jv.kind()) {
        case json::kind::object: {
            os << "{\n";
            indent->append(4, ' ');
            auto const& obj = jv.get_object();
            if (!obj.empty()) {
                auto it = obj.begin();
                for (;;) {
                    os << *indent << json::serialize(it->key()) << " : ";
                    pretty_print(os, it->value(), indent);
                    if (++it == obj.end()) {
                        break;
                    }
                    os << ",\n";
                }
            }
            os << "\n";
            indent->resize(indent->size() - 4);
            os << *indent << "}";
            break;
        }

        case json::kind::array: {
            os << "[\n";
            indent->append(4, ' ');
            auto const& arr = jv.get_array();
            if (!arr.empty()) {
                auto it = arr.begin();
                for (;;) {
                    os << *indent;
                    pretty_print( os, *it, indent);
                    if (++it == arr.end()) {
                        break;
                    }
                    os << ",\n";
                }
            }
            os << "\n";
            indent->resize(indent->size() - 4);
            os << *indent << "]";
            break;
        }

        case json::kind::string:
            os << json::serialize(jv.get_string());
            break;

        case json::kind::uint64:
            os << jv.get_uint64();
            break;

        case json::kind::int64:
            os << jv.get_int64();
            break;

        case json::kind::double_:
            os << jv.get_double();
            break;

        case json::kind::bool_:
            if (jv.get_bool()) {
                os << "true";
            } else {
                os << "false";
            }
            break;

        case json::kind::null:
            os << "null";
            break;
    }

    if (indent->empty()) {
        os << "\n";
    }
}

double get_as_double(json::value &v) {
    switch (v.kind()) {
        case json::kind::uint64:
            return (double) v.get_uint64();

        case json::kind::int64:
            return (double) v.get_int64();

        case json::kind::double_:
            return v.get_double();
        default:
            std::cerr << "Expected this value to be a number: ";
            pretty_print(std::cerr, v);
            exit(EXIT_FAILURE);
    }
}

double get_as_double_or(json::value &v, double default_value) {
    if (v == nullptr) {
        return default_value;
    }
    return get_as_double(v);
}

double get_as_int(json::value &v) {
    return (int) get_as_double(v);
}

double get_as_int_or(json::value &v, int default_value) {
    if (v == nullptr) {
        return default_value;
    }
    return (int) get_as_double(v);
}