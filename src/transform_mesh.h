#ifndef MESHSMITH_BACKEND_TRANSFORM_MESH_H
#define MESHSMITH_BACKEND_TRANSFORM_MESH_H

#include "json.h"
#include "typedefs.h"

void transform_mesh(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_MESH_H
