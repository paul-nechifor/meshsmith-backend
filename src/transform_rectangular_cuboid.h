#ifndef MESHSMITH_BACKEND_TRANSFORM_RECTANGULAR_CUBOID_H
#define MESHSMITH_BACKEND_TRANSFORM_RECTANGULAR_CUBOID_H

#include "json.h"
#include "typedefs.h"

void transform_rectangular_cuboid(Mesh &m, json::value &input);

#endif //MESHSMITH_BACKEND_TRANSFORM_RECTANGULAR_CUBOID_H
