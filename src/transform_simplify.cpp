#include "transform_simplify.h"

#include <CGAL/Surface_mesh_simplification/Policies/Edge_collapse/Count_ratio_stop_predicate.h>
#include <CGAL/Surface_mesh_simplification/edge_collapse.h>

#include "transform_mesh.h"
#include "globals.h"

void transform_simplify(Mesh &mesh, json::value &input) {
    auto obj = input.get_object();

    if (obj["mesh"] == nullptr) {
        std::cerr << "This Simplify should have 'mesh': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    if (obj["stop_ratio"] == nullptr) {
        std::cerr << "This Simplify should have 'stop_ratio': ";
        pretty_print(std::cerr, input);
        exit(EXIT_FAILURE);
    }

    transform_mesh(mesh, obj["mesh"]);

    double stop_ratio = get_as_double(obj["stop_ratio"]);
    CGAL::Surface_mesh_simplification::Count_ratio_stop_predicate<Mesh> stop(stop_ratio);
    int r = CGAL::Surface_mesh_simplification::edge_collapse(mesh, stop);

    if (debug) {
        std::cerr << "Edges removed: " << r << std::endl;
    }
}
