#include <vector>
#include <set>
#include <iostream>
#include "utils_extrusion.h"

struct VecComparator {
    bool operator()(const std::vector<double>& a, const std::vector<double>& b) const {
        if (a.size() != b.size()) {
            return a.size() < b.size();
        }
        for (size_t i = 0; i < a.size(); ++i) {
            if (a[i] != b[i]) {
                return a[i] < b[i];
            }
        }
        return false;
    }
};

bool has_duplicate_points(const std::vector<std::vector<double>>& points) {
    std::set<std::vector<double>, VecComparator> uniquePoints;

    for (const auto& point : points) {
        if (!uniquePoints.insert(point).second) {
            return true;
        }
    }
    return false;
}

void extrude_from_points(Mesh &m, std::vector<std::vector<double>> &points, double height) {
    if (has_duplicate_points(points)) {
        std::cerr << "The extrusion contains duplicate points." << std::endl;
        exit(EXIT_FAILURE);
    }

    for (const auto& point : points) {
        m.add_vertex(K::Point_3(point[0], point[1], 0));
    }

    for (const auto& point : points) {
        m.add_vertex(K::Point_3(point[0], point[1], height));
    }

    m.add_vertex(K::Point_3(0, 0, 0));
    m.add_vertex(K::Point_3(0, 0, height));

    auto n_points = points.size();
    for (int i = 0; i < n_points; i++) {
        // Face
        m.add_face(VI(i), VI((i + 1) % n_points), VI(i + n_points));
        m.add_face(VI((i + 1) % n_points), VI(((i + 1) % n_points) + n_points), VI(i + n_points));
        // Bottom
        m.add_face(VI(i), VI(2 * n_points), VI((i + 1) % n_points));
        // Top
        m.add_face(VI(i + n_points), VI(((i + 1) % n_points) + n_points), VI(2 * n_points + 1));
    }
}

void add_bezier_points(
    std::vector<std::vector<double>> &points,
    int n,
    double p0x,
    double p0y,
    double p1x,
    double p1y,
    double p2x,
    double p2y,
    double p3x,
    double p3y
) {
    // Note: the first point is skipped, because it was included previously.
    for (int i = 1; i <= n; i++) {
        double t = static_cast<double>(i) / n;
        double x = pow(1 - t, 3) * p0x
                   + 3 * pow(1 - t, 2) * t * p1x
                   + 3 * (1 - t) * pow(t, 2) * p2x
                   + pow(t, 3) * p3x;

        double y = pow(1 - t, 3) * p0y
                   + 3 * pow(1 - t, 2) * t * p1y
                   + 3 * (1 - t) * pow(t, 2) * p2y
                   + pow(t, 3) * p3y;

        points.push_back({x, y});
    }
}

void add_quadratic_bezier_points(
    std::vector<std::vector<double>> &points,
    int n,
    double p0x,
    double p0y,
    double p1x,
    double p1y,
    double p2x,
    double p2y
) {
    add_bezier_points(
        points,
        n,
        p0x,
        p0y,
        (1.0/3.0) * p0x + (2.0/3.0) * p1x,
        (1.0/3.0) * p0y + (2.0/3.0) * p1y,
        (2.0/3.0) * p1x + (1.0/3.0) * p2x,
        (2.0/3.0) * p1y + (1.0/3.0) * p2y,
        p2x,
        p2y
    );
}
